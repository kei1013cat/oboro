<!-- contents -->

<footer>
    <h2 class="mt_l"><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/footer_logo.svg" alt="ザンギ専門店 朧 OBORO" class="logo" /></a></h2>
    <address class="pt_s">
        〒062-0904 北海道札幌市豊平区豊平4条3丁目3-30<br>
        TEL 011-598-0401&nbsp;&nbsp;&nbsp;FAX 011-598-0402
    </address>
    <a href="http://www.instahu.com/oboro0401" target="_blank"><img class="pt_s pb_l" src="<?php bloginfo('template_url'); ?>/images/instagram.svg" alt="Instgram" /></a>
    <div class="comp">
        <img class="mb_s" src="<?php bloginfo('template_url'); ?>/images/footer_comp.svg" alt="Instgram" />
        <address>〒245-0009 神奈川県横浜市泉区新橋町690<br>
            TEL 045-719-4227&nbsp;&nbsp;&nbsp;FAX 045-392-9244<br>
            E-mail：nishimura2014@song.ocn.ne.jp
        </address>

    </div>
    <!-- comp -->
    <div class="copy">
        <div class="pc">&copy; 株式会社 にしむら. All rights reserved.</div>
    </div>
    <!-- copy -->
    <nav>

        <ul>
            <li class="sp-menu"><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
            <li class="sp-menu"><a href="<?php bloginfo('url'); ?>/menu/">メニュー</a></li>
            <li class="sp-menu"><a href="<?php bloginfo('url'); ?>/event/">物産展・催事</a></li>
            <li class="sp-menu"><a href="<?php bloginfo('url'); ?>/shop/">店舗紹介</a></li>
            <li class="sp-menu"><a href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></li>
            <li class="sp-menu"><a href="<?php bloginfo('url'); ?>/company/">株式会社にしむら</a></li>
        </ul>
    </nav>
    <!-- wrapper -->
    <div class="sp">&copy; 株式会社 にしむら. All rights reserved.</div>

    <?php if(is_pc()):?>

    <p><a href="#" id="page-top"></a></p>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/top.js"></script>
    <?php endif; ?>
</footer>
</div>
<!-- outer -->


<script>
    <?php if(is_pc()):?>

    jQuery(function($) {
        var ua = navigator.userAgent;
        var $scrtgt = $(window); // スクロール対象：<html>
        if (ua.indexOf('MSIE') > 0 || ua.indexOf('Trident') > 0) {
            $('#page_index .photo1').css('background-attachment', 'scroll');
            $('#page_index .photo2').css('background-attachment', 'scroll');
        } else {
            $('#page_index .photo1').css('background-attachment', 'fixed');
            $('#page_index .photo2').css('background-attachment', 'fixed');
        }
        if (ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0) {
            $('#page_index .photo1').css('background-attachment', 'scroll');
            $('#page_index .photo2').css('background-attachment', 'scroll');
        } else {
            $('#page_index .photo1').css('background-attachment', 'fixed');
            $('#page_index .photo2').css('background-attachment', 'fixed');
        }
    });
    <?php endif; ?>


    $(function() {

        $("#openMenu").click(function() {
            $("#openMenu").hide();
            $("#closeMenu").show();
            $("#layerMenu").show();
            $("#overray").show();
        });
        $("#closeMenu").click(function() {
            $("#closeMenu").hide();
            $("#openMenu").show();
            $("#layerMenu").hide();
            $("#overray").hide();
        });
        $("#layerMenu a").click(function() {
            $("#closeMenu").hide();
            $("#openMenu").show();
            $("#layerMenu").hide();
            $("#overray").hide();
        });
        $("#overray").click(function() {
            $("#closeMenu").hide();
            $("#openMenu").show();
            $("#layerMenu").hide();
            $("#overray").hide();
        });
    });

</script>
<?php wp_footer(); ?>
</div>
<!-- outer -->

</body>

</html>

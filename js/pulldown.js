(function ($) {
	$.dropdown = function (options) {
		var o = $.extend({
			onTarget: 'nav li',
			drop: 'ul.drop',
			onImg: '',
			speed: 'fast' //slow,normal,fast
		}, options);
		$(o.onTarget).hover(
		function () {
			$(o.drop, this).stop(true, true).slideDown(o.speed);
		}, function () {
			$(o.drop, this).slideUp(o.speed);
		});
		$(o.drop).each(function () {
			var img = $(this).parent().find('img:first');
			var src = img.attr('src');
			//var onSrc = src.replace(/\.[^.]+$/, o.onImg + '$&');
			$(this).hover(function () {
				img.attr('src', src);
			}, function () {
				img.attr('src', src);
			});
		});
	},
	$.rollover = function (options) {
		var o = $.extend({
			ovTarget: 'img.over, input.over',
			ovImg: '_ov',
			onImg: '_on'
		}, options);
		$(o.ovTarget).not('[src*="' + o.onImg + '."]').each(function () {
			var src = $(this).attr('src');
			//var ovSrc = src.replace(/\.[^.]+$/, o.ovImg + '$&');
			$(this).hover(function () {
				$(this).attr('src', src);
			}, function () {
				$(this).attr('src', src);
			});
			$(window).unload(function () {
				$(this).attr('src', src);
			});
		});
	};
	$(function () {
		$.dropdown();
		$.rollover();
	});
})(jQuery);
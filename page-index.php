<?php get_header(); ?>

<div id="contents">
  <section id="mainvisual">
    <div class="vegas-outer">
      <div id="vegas-text">
        <p id="vegas-msg">
          <div id="text"></div>
        </p>
      </div>
    </div>
  </section>
  <!-- mainvisual -->

  <!-- お知らせ -->
  <?php
    $wp_query = new WP_Query();
    $param = array(
      'posts_per_page' => '5', //表示件数。-1なら全件表示
      'post_status' => 'publish',
      'orderby' => 'date', //ID順に並び替え
      'order' => 'DESC'
    );
    $wp_query->query($param);?>
  <?php if($wp_query->have_posts()):?>
  <section class="news mt_l pt_l mb_l enter-bottom">
    <div class="wrapper">
    <div class="outer">
      <h3>最新情報</h3>
      <?php while($wp_query->have_posts()) :?>
      <?php $wp_query->the_post(); ?>
      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
      <dl class="cf">
        <dt>
          <?php the_time('Y.m.d'); ?>
        </dt>
        <dd> <?php echo $post->post_title;?> </dd>
      </dl>
      </a>
      <?php endwhile; ?>
    </div>
    </div>
    <!-- wrapper --> 
  </section>
  <?php endif; ?>
  <?php wp_reset_query(); ?>



<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php the_content(); ?>
<?php endwhile; ?>
<?php else : ?>

<?php endif; ?>


</div>
<!--contents -->
<?php get_footer(); ?>

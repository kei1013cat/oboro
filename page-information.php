<?php
/*
Template Name: newslist
*/
?>
<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/part-title.php'); ?>

	<div id="contents">
				<?php
	$paged = (int) get_query_var('paged');
	$wp_query = new WP_Query();
	$param = array(
		'post_status' => 'publish',
		'paged' => $paged,
		'orderby' => 'date', //ID順に並び替え
		'order' => 'DESC',
	);
	$wp_query->query($param);
	?>
		<?php if($wp_query->have_posts()):?>
		<section class="information">
            <div class="wrapper cf">
			<?php while($wp_query->have_posts()) :?>
			<?php $wp_query->the_post(); ?>
			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
			<dl class="cf">
				<dt>
					<?php the_time('Y.m.d'); ?>
				</dt>
				<dd>
					<?php echo $post->post_title;?>
				</dd>
			</dl>
			</a>
			<?php endwhile; ?>

            
            
            
		<div class="pagination">
		    <?php global $wp_rewrite;
		    $paginate_base = get_pagenum_link(1);
		    if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
		        $paginate_format = '';
		        $paginate_base = add_query_arg('paged','%#%');
		    }
		    else{
		        $paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
		        user_trailingslashit('page/%#%/','paged');;
		        $paginate_base .= '%_%';
		    }
		    echo paginate_links(array(
		        'base' => $paginate_base,
		        'format' => $paginate_format,
		        'total' => $wp_query->max_num_pages,
		        'mid_size' => 4,
		        'current' => ($paged ? $paged : 1),
		        'prev_text' => '« 前へ',
		        'next_text' => '次へ »',
		    )); ?>
		</div><!-- pagination -->
            
            
            
</div>
<!-- wrapper -->
            
		</section>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
	</div>
	<!-- contents -->


<?php get_footer(); ?>

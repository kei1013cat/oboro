<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php
  $url = $_SERVER['REQUEST_URI'];
?>
<meta name="format-detection" content="telephone=no">
<?php if(is_pc()): ?>
<meta content="width=1000" name="viewport">
<?php else: ?>
<meta name="viewport" content="width=device-width">
<?php endif; ?>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>
<?php if(is_page() && $post->post_name != 'home'){ wp_title('');echo ' | '; } ?>
<?php bloginfo('name'); ?>
</title>
<meta name="keywords" content="朧,おぼろ,ザンギ,おにぎり,おにぎらず,手作り,催事,物産展,日本全国,店舗,テイクイン" />
    
<?php wp_head(); ?>
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/sp.css?v=20180109" media="screen and (max-width: 767px)">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/pc.css?v=20180109" media="screen and (min-width: 768px)">

<!-- favicon -->    
<link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/images/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="<?php bloginfo('template_url'); ?>/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">    
    
<script src="<?php bloginfo('template_url'); ?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>


<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/pulldown.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/rollover.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/sp_switch.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>

<!-- scrollreveal -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.thema.js"></script>

<?php if(is_pc()):?>
<!--[if lt IE 9]>
<script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
<![endif]-->
<?php endif; ?>


    
<!-- スライダー(vegas)-->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/vegas/vegas.css">
<script src="<?php bloginfo('template_url'); ?>/js/vegas/vegas.js"></script>
<script>
$(function(){
$('#mainvisual').vegas({
    slides: [
        { src: 'wp-content/themes/oboro/images/mainimage.jpg?v=20190215',msg:'<h2 class="text1"><img src="wp-content/themes/oboro/images/mainimage_text.svg" alt=""></h2>'},
        { src: 'wp-content/themes/oboro/images/mainimage2.jpg?v=20190215',msg:'<h2 class="text2"><img src="wp-content/themes/oboro/images/mainimage_text2.svg" alt=""></h2>'},
    ],
        transition: 'fade2',
        transitionDuration: 1500,
        delay: 10000,
        animationDuration: 18000,
        timer:false
	});
});
</script>

</head>
<?php
	$body_id = "";
	if ( is_home() ) {
		$body_id = ' id="page_index"';
	}else if ($post->post_type == 'jirei') {
		$body_id = ' id="page_'.$post->post_type.'" class="subpage"';
	}else if ( is_page() ) {
		$body_id = ' id="page_'.get_post($post->post_parent)->post_name.'" class="subpage"';
	}else if (  is_post_type_archive('works') || is_singular('works') ) {
		$body_id = ' id="page_works" class="subpage"';
	}else if ( is_single() ) {
		$body_id = ' id="page_single" class="subpage"';
	}else if ( is_archive() ) {
		$body_id = ' id="page_archive" class="subpage"';
	}
?>

<body<?php echo $body_id; ?>>

<div id="outer">
<header class="global cf">

<?php
    $parent_id = $post->post_parent; // 親ページのIDを取得
    $parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得
?>

<div class="pc">
    <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.svg" alt="ザンギ専門店 | 朧 | OBORO" /></a></h1>
    <nav>
      <ul class="cf">
    　　<li><a <?php if(is_front_page()): ?> class="hover"<?php endif; ?> href="<?php bloginfo('url'); ?>/" >ホーム</a></li>
       <li><a <?php if($post->post_name =='menu'): ?> class="hover"<?php endif; ?> href="<?php bloginfo('url'); ?>/menu/" >メニュー</a></li>
       <li><a <?php if($post->post_name =='event'): ?> class="hover"<?php endif; ?> href="<?php bloginfo('url'); ?>/event/" >物産展・催事</a></li>
       <li><a <?php if($post->post_name =='shop'): ?> class="hover"<?php endif; ?> href="<?php bloginfo('url'); ?>/shop/" >店舗紹介</a></li>
       <li><a <?php if($post->post_name =='contact'): ?> class="hover"<?php endif; ?> href="<?php bloginfo('url'); ?>/contact/" >お問い合わせ</a></li>
       <li class="comp"><a <?php if($post->post_name =='company'): ?> class="hover"<?php endif; ?> href="<?php bloginfo('url'); ?>/company/" >株式会社<span class="b">にしむら</span></a></li>
      </ul>
    </nav>
</div>
<div class="sp-menu sp">

  <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.svg" alt="ザンギ専門店 | 朧 | OBORO" /></a></h1>

  <div class="menu cf">
    <p id="closeMenu"> <a href="#"><img src="<?php bloginfo('template_url'); ?>/images/header_close.svg" ></a> </p>
    <p id="openMenu"> <a href="#"> <img src="<?php bloginfo('template_url'); ?>/images/header_menu.svg"> </a> </p>
  </div>
  <!-- menu -->
  <div id="layerMenu">
    <nav>
      <ul>
       <li><a href="<?php bloginfo('url'); ?>/" >ホーム</a></li>
       <li><a href="<?php bloginfo('url'); ?>/menu/" >メニュー</a></li>
       <li><a href="<?php bloginfo('url'); ?>/event/" >物産展・催事</a></li>
       <li><a href="<?php bloginfo('url'); ?>/shop/" >店舗紹介</a></li>
       <li><a href="<?php bloginfo('url'); ?>/contact/" >お問い合わせ</a></li>
       <li><a href="<?php bloginfo('url'); ?>/company/" >株式会社にしむら</a></li>
      </ul>
    </nav>
  </div>
  <!-- layerMenu -->
  <div id="overray"> </div>

</div>
</header>
<div id="main_contents" class="cf">
